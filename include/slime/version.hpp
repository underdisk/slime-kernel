////////////////////////////////////////////////////////
//                                                    //
//    SLIME KERNEL                                    //
//                                                    //
//    This Source Code Form is subject to the terms   //
//    of the Mozilla Public License, v. 2.0.          //
//                                                    //
//    If a copy of the MPL was not distributed with   //
//    this file, You can obtain one at                //
//    http://mozilla.org/MPL/2.0/.                    //
//                                                    //
////////////////////////////////////////////////////////

#pragma once

#include "types.hpp"

constexpr int MAJOR = 0;
constexpr int MINOR = 1;
constexpr int PATCH = 0;

namespace kernel
{
	struct version_info
	{
		u8 major_version;
		u8 minor_version;
		u8 patch_version;
	};

	struct version_info_char
	{
		char major_version;
		char minor_version;
		char patch_version;
	};

	version_info get_version()
	{
		return {MAJOR, MINOR, PATCH};
	}

	version_info_char get_version_char()
	{
		return {MAJOR + '0', MINOR + '0', PATCH + '0'};
	}
}