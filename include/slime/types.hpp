////////////////////////////////////////////////////////
//                                                    //
//    SLIME KERNEL                                    //
//                                                    //
//    This Source Code Form is subject to the terms   //
//    of the Mozilla Public License, v. 2.0.          //
//                                                    //
//    If a copy of the MPL was not distributed with   //
//    this file, You can obtain one at                //
//    http://mozilla.org/MPL/2.0/.                    //
//                                                    //
////////////////////////////////////////////////////////

#pragma once

typedef unsigned char u8;
typedef unsigned short int u16;
typedef unsigned int u32;
typedef unsigned long int u64;

typedef signed char i8;
typedef signed short int i16;
typedef signed int i32;
typedef signed long int i64;

typedef float f32;