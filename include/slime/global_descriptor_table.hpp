////////////////////////////////////////////////////////
//                                                    //
//    SLIME KERNEL                                    //
//                                                    //
//    This Source Code Form is subject to the terms   //
//    of the Mozilla Public License, v. 2.0.          //
//                                                    //
//    If a copy of the MPL was not distributed with   //
//    this file, You can obtain one at                //
//    http://mozilla.org/MPL/2.0/.                    //
//                                                    //
////////////////////////////////////////////////////////

#pragma once

#include "types.hpp"

namespace kernel
{
	class GlobalDescriptorTable
	{
		public:
			class SegmentDescriptor
			{
				private:
					u16	m_limit_lo;
					u16	m_base_lo;
					u16	m_base_hi;
					u8	m_type;
					u8	m_flags_limit_hi;
					u8	m_base_vhi;

				public:
					SegmentDescriptor(u32 base, u32 limit, u8 flags);
					u32 base();
					u32 limit();

			}; __attribute__((packed));

			SegmentDescriptor nullSegmentSelector;
			SegmentDescriptor unusedSegmentSelector;
			SegmentDescriptor codeSegmentSelector;
			SegmentDescriptor dataSegmentSelector;

		public:

		GlobalDescriptorTable();
		~GlobalDescriptorTable();

		u16 getCodeSegmentSelector();
		u16 getDataSegmentSelector();
	};
} // namespace kernel