////////////////////////////////////////////////////////
//                                                    //
//    SLIME KERNEL                                    //
//                                                    //
//    This Source Code Form is subject to the terms   //
//    of the Mozilla Public License, v. 2.0.          //
//                                                    //
//    If a copy of the MPL was not distributed with   //
//    this file, You can obtain one at                //
//    http://mozilla.org/MPL/2.0/.                    //
//                                                    //
////////////////////////////////////////////////////////

#pragma one

#include "types.hpp"

namespace kernel { namespace real_mode_display
{
	void print_str(const char* text, u32 color = 0x3, u32 offset = 0x0)
	{
		char* mem = (char*)0xb8000 + (offset * 2);
		while(*text)
		{
			*mem++ = *text++;
			*mem++ = color;
		}
	}	

	void clear()
	{
		char* mem = (char*)0xb8000;
		int i(0);
		while (i < 80 * 25 * 2)
			mem[i++] = 0;
	}

}} //namespace kernel::real_mode_display