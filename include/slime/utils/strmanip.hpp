////////////////////////////////////////////////////////
//                                                    //
//    SLIME KERNEL                                    //
//                                                    //
//    This Source Code Form is subject to the terms   //
//    of the Mozilla Public License, v. 2.0.          //
//                                                    //
//    If a copy of the MPL was not distributed with   //
//    this file, You can obtain one at                //
//    http://mozilla.org/MPL/2.0/.                    //
//                                                    //
////////////////////////////////////////////////////////

#pragma once

#include <slime/types.hpp>

namespace kernel { namespace utils
{
	//swaps char 'a' and 'b'
	inline void swap(char* a, char* b)
	{
		char t = *a;
		*a = *b;
		*b = t;
	}

	//reverses elements i to j in 'buf'
	//returns 'buf'
	//WARNING: be sure that i and j are smaller than the buffer size
	char* strreverse(char* buf, u32 end, u32 start = 0)
	{
		while (start < end)
			swap(&buf[start++], &buf[end--]);
		return buf;
	}

	//returns the length of a null-terminated string
	//WARNING: be sure that str in null-terminated
	u32 strlen(const char* str)
	{
		u32 r = 0;
		while (str[r])
			++r;
		return r;
	}

	//fills a string from 'offset' for 'len' bytes
	//WARNING: be sure that offset + len isn't bigger than 'str' size
	void strfill(char* str, u32 len, char c, u32 offset = 0)
	{
		u32 s = len + offset;
		for (u32 i = offset; i < s; ++i)
			str[i] = c;
	}

	//concatenates null-terminated string 'str1'
	//		   and null-terminated string 'str2', puts the result into 'recipient' and null terminates it
	//WARNING: be sure that 'recipient' has enough bytes
	char* strcat(char *dest, const char *src)
	{
		u32 i,j;
		for (i = 0; dest[i] != '\0'; i++)
			;
		for (j = 0; src[j] != '\0'; j++)
			dest[i+j] = src[j];
		dest[i+j] = '\0';
		return dest;
	}

	//converts str (null-terminated ASCII string) to T (number)
	//returns number representation of str if str is correct, undefined behaviour otherwise
	template <typename T>
	T to_number(char* str)
	{
		T p = 0;
		while (*str)
		{
			p = (p << 3) + (p << 1) + (*str) - '0';
			str++;
		}
		return p;
	}
}}