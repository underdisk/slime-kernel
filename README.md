![alt text](https://i.imgur.com/pYvOuQA.png "Slime Kernel 0.1.0 Screenshot")

# slime kernel

My first approach to osdev.
Thanks to : https://www.youtube.com/watch?v=1rnA6wpF0o4&list=PLHh55M_Kq4OApWScZyPl5HhgsTJS9MZ6M

You will need :
- g++
- binutils
- libc6-dev-i386
- grub-legacy
- xorriso

(all available using apt-get on debian-based GNU/Linux distributions)

to build just do 'make',
it should create kernel.bin/.iso files. There you have it.

It's the first time I try osdev stuff, donc expect something revolutionnary.

# it is not booting!

If the output is a ~300kb iso file, it won't boot! 

as described [here](https://wiki.osdev.org/Bare_Bones) 

> If your development system is booted from EFI it may be that you don't have the PC-BIOS version of the grub binaries installed anywhere. 

The fix is to install those packages :

- grub-pc-bin

(all available using apt-get on debian-based GNU/Linux distributions)
