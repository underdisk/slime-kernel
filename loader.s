.set MAGICNBR, 0x1badb002; #kernel magic number
.set FLAGS, (1<<0 | 1<<1)
.set CHECKSUM, -(MAGICNBR + FLAGS)

.section .multiboot
	.long MAGICNBR
	.long FLAGS
	.long CHECKSUM

.section .text
.extern kmain
.extern call_ctors
.global loader

loader:
	mov $kernel_stack, %esp

	call call_ctors

	push %eax
	push %ebx
	call kmain

_stop:
	cli
	hlt
	jmp _stop

.section .bss
.space 2*1024*1024; # 2 MiB
kernel_stack:
