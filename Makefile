
CXX = g++
GPPPARAMS = -m32 -std=c++17 -fno-use-cxa-atexit -nostdlib -fno-builtin -fno-rtti -fno-exceptions -fno-leading-underscore
ASPARAMS = --32
LDPARAMS = -melf_i386

INCLUDE_DIRS = include
SRC_DIRS = src
ODIR = obj
BINDIR = bin

_objects = loader.o global_descriptor_table.o kernel.o
objects = $(patsubst %,$(ODIR)/%,$(_objects))

all: $(BINDIR)/kernel.iso


noleftover: all
	rm -rf $(ODIR) iso

$(ODIR):
	mkdir $(ODIR)

$(BINDIR):
	mkdir $(BINDIR)

$(ODIR)/%.o: $(SRC_DIRS)/%.cpp $(ODIR)
	$(CXX) $(GPPPARAMS) -I $(INCLUDE_DIRS) -o $@ -c $<

$(ODIR)/%.o: %.s $(ODIR)
	as $(ASPARAMS) -o $@ $<

$(BINDIR)/kernel.bin: linker.ld $(objects) $(BINDIR)
	ld $(LDPARAMS) -T $< -o $@ $(objects)

install: $(BINDIR)/kernel.bin $(BINDIR)
	sudo cp $< /boot/kernel.bin

$(BINDIR)/kernel.iso: $(BINDIR)/kernel.bin $(BINDIR)
	mkdir iso
	mkdir iso/boot
	mkdir iso/boot/grub
	cp $< iso/boot/
	echo 'set timeout=0' > iso/boot/grub/grub.cfg
	echo 'set default=0' >> iso/boot/grub/grub.cfg
	echo '' >> iso/boot/grub/grub.cfg
	echo 'menuentry "SlimeOS" {' >> iso/boot/grub/grub.cfg
	echo '	multiboot /boot/kernel.bin' >> iso/boot/grub/grub.cfg
	echo '	boot' >> iso/boot/grub/grub.cfg
	echo '}' >> iso/boot/grub/grub.cfg
	grub-mkrescue --output=$@ iso

clean:
	rm -rf $(ODIR) $(BINDIR) iso
