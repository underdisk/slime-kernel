////////////////////////////////////////////////////////
//                                                    //
//    SLIME KERNEL                                    //
//                                                    //
//    This Source Code Form is subject to the terms   //
//    of the Mozilla Public License, v. 2.0.          //
//                                                    //
//    If a copy of the MPL was not distributed with   //
//    this file, You can obtain one at                //
//    http://mozilla.org/MPL/2.0/.                    //
//                                                    //
////////////////////////////////////////////////////////

#include <slime/version.hpp>
#include <slime/real_mode_display.hpp>
#include <slime/utils/strmanip.hpp>
#include <slime/global_descriptor_table.hpp>

typedef void (*constructor)();

extern "C"
{
	constructor start_ctors;
	constructor end_ctors;
	void call_ctors()
	{
		for (constructor* i = &start_ctors; i != &end_ctors; ++i)
		{
			(*i)(); //call ctors
		}
	}

	void kmain(void* multiboot_structure, unsigned int magic_number)
	{
		u64 timer;
		kernel::version_info_char version = kernel::get_version_char();
		kernel::real_mode_display::clear();

		char time_indicator_array[] = "|/-\\|/-\\";
		char time_indicator[2] = {
			'M',
			'\0'
		};

		char version_str[] = {version.major_version, '.', version.minor_version, '.', version.patch_version, '\0'};
		char str[64] = "The Slime Kernel, version ";

		kernel::utils::strcat(str, version_str);

		kernel::GlobalDescriptorTable gdt;
		
		while (true)
		{
			kernel::real_mode_display::print_str(time_indicator, 0x6);
			kernel::real_mode_display::print_str(str, 0x2, 2);

			++timer;
			time_indicator[0] = time_indicator_array[timer % 8];
		}
	}

}
